$( document ).ready(function() {

  function toggleDelivery(paymentmethod){

     if(paymentmethod == "payment_offline_pickup"){   

      $(".shipping_details").hide();
     }else{
      $(".shipping_details").show();
     }
  }

  $("input[name=var_payment]:radio").change(function () {
   toggleDelivery(this.value);
  });

  toggleDelivery($("input[name=var_payment]:checked").val());

});
